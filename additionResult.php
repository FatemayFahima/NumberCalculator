<?php
include_once "vendor/autoload.php";

use Pondit\Calculator\NumberCalculator\Addition;
use Pondit\Calculator\NumberCalculator\Displayer;

$addition1=new Addition();
$addition1->number1=4;
$addition1->number2=5;

$displayer1=new Displayer();
$displayer1->displaypre($addition1->getAddition());
