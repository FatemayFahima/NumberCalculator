<?php
include_once "vendor/autoload.php";

use Pondit\Calculator\NumberCalculator\Multiplication;
use Pondit\Calculator\NumberCalculator\Displayer;

$multiplication1=new Multiplication();
$multiplication1->number1=5;
$multiplication1->number2=4;

$displayer=new Displayer();
$displayer->displaypre($multiplication1->getMultiplication());