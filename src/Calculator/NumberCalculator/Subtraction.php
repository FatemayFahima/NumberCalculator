<?php

namespace Pondit\Calculator\NumberCalculator;


class Subtraction
{
   public $number1;
   public $number2;
   public function getSubtraction(){
       return $this->number1-$this->number2;
   }
}