<?php
include_once "vendor/autoload.php";

use Pondit\Calculator\NumberCalculator\Division;
use Pondit\Calculator\NumberCalculator\Displayer;

$division1=new Division();
$division1->number1=30;
$division1->number2=3;

$displayer1=new Displayer();
$displayer1->displaypre($division1->getDivision());