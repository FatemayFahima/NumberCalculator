<?php
include_once "vendor/autoload.php";

use Pondit\Calculator\NumberCalculator\Subtraction;
use Pondit\Calculator\NumberCalculator\Displayer;

$subtraction1=new Subtraction();
$subtraction1->number1=13;
$subtraction1->number2=7;

$displayer1=new Displayer();
$displayer1->displaypre($subtraction1->getSubtraction());